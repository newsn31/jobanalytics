# README #

This is a job data analytic tool I created.  It implements a basic web crawler to parse the last 50 job postings of the website indeed.com into a MongoDB collection where they can be analyzed.

### Demo: ###
[https://www.youtube.com/watch?v=J80ryxbvj6w](https://www.youtube.com/watch?v=J80ryxbvj6w)

### Who do I talk to? ###

quang.vo31@gmail.com