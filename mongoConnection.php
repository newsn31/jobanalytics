<?php

	require_once('databaseVariables.php');	
	try {
		$connection = new Mongo($DBHOST, $PORTNUMBER);
		$collection= connection->selectCollection($DATABASE, $DBCOLLECTION);
		
		$document = array{
			'URL' => $link,
			'content' => $input,
			'saved_at' => new MongoDate();
		};
		$collection->insert($document);
		}
	catch (MongoConnectionException $e) {
		die("Failed to connect to database ".$e->getMessage());
	}
	catch (MongoException $e){
		die('Failed to insert data '.$e->getMessage());
	}
?>